# SPECS Chef Repository - Enabling Platform

The Enabling Platform repository consists of a set of cookbooks to be used to deploy, configure and control the Enabling Platform components using [Chef technology](http://chef.io/).

The Enabling Platform components host the SPECS Core Components. SPECS Core Components orchestrate the entire functionality of the SPECS Platform.

# Repository structure

The Chef Repository contains a set of directories where all the cookbooks are described. 
Each cookbook must have its own directory and the name of the directory represents the name of the cookbook. The cookbook must be define in accordance with [Chef cookbooks guidelines](https://docs.chef.io/cookbook_repo.html)

## Default cookbook structure

* specs-core-enabling_platform-repository/
  * cookbook_name/
    * attributes/
    * files/
    * libraries/
    * providers/
    * recipes/
    * resources/
    * metadata.rb
    * README.md

ATTENTION: **metadata.rb** must contain the cookbook description and it is mandatory to be created (otherwise the cookbook will be rejected by the Chef Server/Client tools).

# Repository cookbooks

The current Enabling Platform cookbooks are:

* **applications/** -- SPECS Applications
* **enabling-platform/** -- SPECS Enabling-Platform components (mOS, Chef Server, Chef Clients etc.)
* **enforcement/** -- SPECS Core Components - Enforcement;
* **monitoring/** -- SPECS Core Components - Monitoring;
* **sla-negotiation/** -- SPECS Core Components - SLA Negotiation;
* **sla-platform/** -- SPECS Core Components - SLA Platform
* **template/** -- SPECS Cookbook template - to be used as an example for further SPECS Cookbooks;

# Repository Launcher descriptor

In order to expose a cookbook recipes to the Launcher a descriptor need to be created in the root directory of the current repository:

* specs-core-enabling_platform-repository/
  * repository_descriptor.json

The descriptor has the following structure:

```
#!json

{ 
  "name" : "specs-core-enabling_platform-repository",
  "version" : "0.0.1",
  "cookbooks" : {
     "cookbook_name": {
	"version": "0.0.1",
	"description": "Coobook Name Description",
	"default": "default-recipe",
	"recipes": ["default-recipe","recipe1", "recipeN"]
     },
  }
}
```