#
# Cookbook Name:: applications
# Recipe:: web-container-app
#
# Copyright 2010-2015, IeAT, Romania, http://ieat.ro/
# Copyright 2010-2015, CeRICT, Italia, http://cerict.it/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "CeRICT", http://url/ .
#
# Developers:
#  * Massimiliano Rak <massimiliano.rak@unina2.it>
#  * Silviu Panica <silviu.panica@e-uvt.ro>
#

include_recipe "enabling-platform::apache-tomcat-v7"

remote_file '/opt/mos-apache-tomcat-v7/webapps/webcontainer-app-rev2.war' do
  source 'http://ftp.specs-project.eu/public/artifacts/applications/secure-web-container/webcontainer-app-rev2-0.0.1-SNAPSHOT.war'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  only_if { ::File.exists?("/opt/mos-apache-tomcat-v7/cmd/bootstrap")  and ! ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/webcontainer-app-rev2") }
end

# Edit webcontainer_app.properties configuration file

databagarray = search("ip_config","id:1")

if databagarray == nil then
puts "Databag is not present"
end

databag = databagarray.first
template "/opt/mos-apache-tomcat-v7/webapps/specs_app.properties" do
action :create
source "specs_app_properties.erb"
variables(
      :databag => databag
      )
      not_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/specs_app.properties") }
end


# register the component into the DNS (TODO: call this only if service startup is successful)
# works only on mOS 4.x and upper. If you are using a different operating system please remove 
# everything below this line.
#
hostname='secure-web-container.applications'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
    returns [0, 1]
end
## end register into the DNS