#
# Cookbook Name:: credential-management-application
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'enabling-platform::apache-tomcat-v7'
cookbook_file "/opt/mos-apache-tomcat-v7/webapps/JSPVault.war" do
 source "JSPVault.war"
 mode 0777
 not_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/JSPVault") }
end
cookbook_file "/opt/mos-apache-tomcat-v7/webapps/Component.war" do
 source "Component.war"
 mode 0777
 not_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/Component") }
end
cookbook_file "/opt/mos-apache-tomcat-v7/webapps/frontend-Credential_Manager.war" do
    source "frontend-Credential_Manager.war"
    mode 0777
    not_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/frontend-Credential_Manager") }
end
cookbook_file "/opt/credentials2.json" do
 source "credentials2.json"
 mode 0777
end
