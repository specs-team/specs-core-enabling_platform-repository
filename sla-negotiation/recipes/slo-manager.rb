#
# Cookbook Name:: sla-negotiation
# Recipe:: slo-manager-api
#
# Copyright 2010-2015, Company, Country, http://url/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * Silviu Panica <silviu.panica@e-uvt.ro>
#

package "mos-mongodb-org"

include_recipe "enabling-platform::apache-tomcat-v7"
include_recipe "sla-platform::sla-manager"
include_recipe "sla-platform::service-manager"
include_recipe "enforcement::planning"

# start the service using Systemd
service "mongod_start" do
  provider Chef::Provider::Service::Systemd
  service_name "mongod"
  action :start
end

remote_file '/opt/mos-apache-tomcat-v7/webapps/slo-manager-api.war' do
  source 'http://ftp.specs-project.eu/public/artifacts/sla-negotiation/slo-manager/slomanager-api-0.0.1-SNAPSHOT.war'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  only_if { ::File.exists?("/opt/mos-apache-tomcat-v7/cmd/bootstrap")  and ! ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/slo-manager-api") }
end

ruby_block 'wait-for-war-deployment' do
  block do
    true until ::File.exists?('/opt/mos-apache-tomcat-v7/webapps/slo-manager-api/WEB-INF/classes')
  end
end

cookbook_file "slomanager.properties" do
  only_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/slo-manager-api/WEB-INF/classes/slomanager.properties") }
  path "/opt/mos-apache-tomcat-v7/webapps/slo-manager-api/WEB-INF/classes/slomanager.properties"
  action :create
end
# register the event-hub into the DNS (TODO: call this only if service startup is successful)
# works only on mOS 4.x and upper. If you are using a different operating system please remove 
# everything below this line.
#
hostname='slo-manager.sla-negotiation.services'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
    returns [0, 1]
end
## end register into the DNS
