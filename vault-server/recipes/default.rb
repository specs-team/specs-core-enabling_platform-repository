#
# Cookbook Name:: vault-server
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'mysql::default'
script_file = "#{Chef::Config['file_cache_path']}/mysql.sh"
cookbook_file "/tmp/mysql.sh" do
source "mysql.sh"
mode 0777
end
cookbook_file "/opt/vault" do
source "vault"
mode 0777
end
cookbook_file "/opt/config.hcl" do
source "config.hcl"
mode 0755
end
execute "yolo" do
 command "nohup sudo sh /tmp/mysql.sh &>/dev/null &"
 end