name             'sla-platform'
maintainer       'SPECS Project -- SLA Platform'
maintainer_email 'support@specs-project.eu'
license          'Apache 2.0'
description      'Chef Cookbook to manage the deployment of various SPECS Platform Components'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

depends 'sla-negotiation'
depends 'enabling-platform'
depends 'enforcement'
depends 'applications'