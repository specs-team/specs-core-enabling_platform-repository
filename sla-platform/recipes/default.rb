#
# Cookbook Name:: sla-platform
# Recipe:: default
#
# Copyright 2010-2015, IeAT, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * Silviu Panica <silviu.panica@e-uvt.ro>
#

include_recipe "sla-platform::sla-manager"
include_recipe "sla-platform::service-manager"
include_recipe "sla-negotiation::slo-manager"
include_recipe "enforcement::planning"
include_recipe "sla-platform::metric-catalogue"
include_recipe "applications::metric-catalogue-app"

execute 'start-mos-apache-tomcat-v7' do
  not_if do 
    ::File.exists?("/var/run/mos-apache-tomcat-v7.pid")
  end
  command '/opt/mos-apache-tomcat-v7/cmd/bootstrap'
  action :run
end

# register the component into the DNS (TODO: call this only if service startup is successful)
# works only on mOS 4.x and upper. If you are using a different operating system please remove 
# everything below this line.
#
hostname='platform.sla-platform.services'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
    returns [0, 1]
end
## end register into the DNS
