#
# Cookbook Name:: monitoring
# Recipe:: wui
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#

package "specs-monitoring-wui"
include_recipe "monitoring::event-hub"
include_recipe "monitoring::monipoli"

service "moshttpd_restart" do
  provider Chef::Provider::Service::Systemd
  service_name "moshttpd"
  action :restart
end

ruby_block "mos_httpd_update" do
  block do
  open('/opt/mos-node-bootstrapper/etc/mos-node-httpd.conf', 'a') { |f|
    f << '
server.modules += ("mod_proxy", "mod_setenv")
$HTTP["url"] =~ "^.*streams" {
  setenv.add-response-header = ( "Access-Control-Allow-Origin" => "*" )
  proxy.server  = ( "" =>
    (( "host" => "127.0.0.1", "port" => 9090 ))
  )
}
$HTTP["url"] =~ "^.*events" {
  setenv.add-response-header = ( "Access-Control-Allow-Origin" => "*" )
  proxy.server  = ( "" =>
    (( "host" => "127.0.0.1", "port" => 9090 ))
  )
}
$HTTP["url"] =~ "^.*monipoli" {
  setenv.add-response-header = ( "Access-Control-Allow-Origin" => "*" )
  proxy.server  = ( "" =>
    (( "host" => "127.0.0.1", "port" => 5000 ))
  )
}

'
  }
  end
  only_if { ::File.exists?("/opt/mos-node-bootstrapper/etc/mos-node-httpd.conf") and
            ! File.open('/opt/mos-node-bootstrapper/etc/mos-node-httpd.conf').read().include? "Access-Control-Allow-Origin"
          }
  notifies :restart, 'service[moshttpd_restart]', :immediately
end


# register the event-hub into the DNS (TODO: call this only if service startup is successful)
#
hostname='wui.monitoring.services'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
    returns [0, 1]
end
## end register into the DNS
