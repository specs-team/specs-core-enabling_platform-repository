#
# Cookbook Name:: monitoring
# Recipe:: ctp-server-dev
#
# Copyright 2010-2015, CSA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "CSA"
#
# Developers:
#  * CSA
#

package "specs-monitoring-ctp-server-dev"

# prepare the service with custom files from cookbook file repository
cookbook_file "specs-monitoring-ctp-server-mongodb.js" do
   not_if do ::File.exists?("/opt/specs-monitoring-ctp-server-dev/etc/build-db.js") end
   path "/opt/specs-monitoring-ctp-server-dev/etc/build-db.js"
   mode '0644'
   owner 'root'
   group 'root'
   action :create
end

# start the service using Systemd (applicable only for services that have already a systemd unit file registered)
# default listen port: 7070
service "specs-monitoring-ctp-server_start" do
  pattern "/opt/specs-monitoring-ctp-server-dev/bin/ctpd"
  provider Chef::Provider::Service::Systemd
  service_name "specs-monitoring-ctp-server"
  action :start, :immediately
  supports :restart => true
end

# create mongodb layout
bash "ctp_mongodb_setup" do
  user "root"
  cwd "/tmp"
  code <<-EOF
    mongo /opt/specs-monitoring-ctp-server-dev/etc/build-db.js
    touch /opt/specs-monitoring-ctp-server-dev/var/run/db-set
  EOF
  not_if do ::File.exists?("/opt/specs-monitoring-ctp-server-dev/var/run/db-set") end
  notifies :restart, 'service[specs-monitoring-ctp-server_start]', :immediately
end


# register the event-hub into the DNS (TODO: call this only if service startup is successful)
#
hostname='ctp-server.monitoring.services'
#
### DO NOT MODIFY BELOW THIS LINE
bash "ns_register" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /mos/lib/mos/tools.sh ];then
        source /mos/lib/mos/tools.sh
        ns_register_service A #{hostname}
    fi
    EOH
    not_if "source /mos/lib/mos/tools.sh; ns_check_service #{hostname}"
    returns [0, 1]
end
## end register into the DNS
