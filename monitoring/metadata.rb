name             'monitoring'
maintainer       'SPECS Project -- Core Components/Monitoring'
maintainer_email 'support@specs-project.eu'
license          'Apache 2.0'
description      'Chef Cookbook to manage the deployment of various SPECS Platform Components'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.3'
