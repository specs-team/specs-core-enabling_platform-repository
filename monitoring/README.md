SPECS Project -- Template
=========================

This cookbook will offer various recipes used by the SPECS Enabling Platform

Requirements
------------

mOS Operating System - version 4.0.x

License and Authors
-------------------

Apache License 4.0 applies for all the content if not otherwise specified.
